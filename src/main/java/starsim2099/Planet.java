package starsim2099;

/**
 * Write a description of class Planet here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Planet extends Position {
	// Attribute
	private int hafen;
	private String name;

	// Methoden
	public int getHafen() {
		return hafen;
	}

	public String getName() {
		return name;
	}

	public void setHafen(int hafen) {
		this.hafen = hafen;
	}

	public void setName(String name) {
		this.name = name;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] planetShape = { { '\0', '/', '*', '*', '\\', '\0' }, { '|', '*', '*', '*', '*', '|' },
				{ '\0', '\\', '*', '*', '/', '\0' } };
		return planetShape;

	}
}
