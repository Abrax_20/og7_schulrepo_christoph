package starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Ladung extends Position {

	// Attribute
	private int masse;
	private String typ;

	// Methoden
	public String getTyp() {
		return typ;
	}

	public int getMasse() {
		return masse;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public void setMasse(int masse) {
		this.masse = masse;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}


}