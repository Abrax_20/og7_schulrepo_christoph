import experimentprimzahlenAlgorithmuss.Timer;

public class MergeSort {
    public static int[] sort(int[] array, int l, int r) {
        if (l < r) {
            int q = (l + r) / 2;

            array = sort(array, l, q);
            array = sort(array,q + 1, r);
            array = merge(array, l, q, r);
        }
        return array;
    }

    public static int[] merge(int[]array, int l, int q, int r) {
        int[] arr = new int[array.length];
        int i, j;
        for (i = l; i <= q; i++) {
            arr[i] = array[i];
        }
        for (j = q + 1; j <= r; j++) {
            arr[r + q + 1 - j] = array[j];
        }
        i = l;
        j = r;
        for (int k = l; k <= r; k++) {
            if (arr[i] <= arr[j]) {
                array[k] = arr[i];
                i++;
            } else {
                array[k] = arr[j];
                j--;
            }
        }

        return array;
    }

    public static int[] generateList(int lenght) {
        int[] list = new int[lenght];
        for (int index = 0; index < list.length; index++) {
            list[index] = lenght - (index+1);
        }

        return list;
    }

    public static void main(String[] args) {
        int[] array;
        Timer timer = new Timer();

        int[] array1 = { 0, 1, 2 };
        timer.start();
        array1= sort(array1, 0, array1.length - 1);
        timer.stop();
        System.out.println("0 <-> 1   { z y x }:" + timer.getDifferenz());

        int[] array2 = { 1, 0, 2 };
        timer.start();
        array2= sort(array2, 0, array2.length - 1);
        timer.stop();
        System.out.println("0 <-> 1   { y z x }:" + timer.getDifferenz());

        int[] array3 = { 1, 2, 0 };
        timer.start();
        array3 = sort(array3, 0, array3.length - 1);
        timer.stop();
        System.out.println("0 <-> 1   { y x z }:" + timer.getDifferenz());

        int[] array4 = { 2, 1, 0 };
        timer.start();
        array4= sort(array4, 0, array4.length - 1);
        timer.stop();
        System.out.println("0 <-> 1   {  x y z }:" + timer.getDifferenz());

        array = generateList(10);
        timer.start();
        array = sort(array, 0, array.length - 1);
        timer.stop();
        System.out.println("n=10: " + timer.getDifferenz());
        array = generateList(100);
        timer.start();
        array = sort(array, 0, array.length - 1);
        timer.stop();
        System.out.println("n=100: " + timer.getDifferenz());
        array = generateList(1000);
        timer.start();
        array = sort(array, 0, array.length - 1);
        timer.stop();
        System.out.println("n=1000: " + timer.getDifferenz());
        array = generateList(10000);
        timer.start();
        array = sort(array, 0, array.length - 1);
        timer.stop();
        System.out.println("n=10000: " + timer.getDifferenz());
        array = generateList(100000);
        timer.start();
        array = sort(array, 0, array.length - 1);
        timer.stop();
        System.out.println("n=100000: " + timer.getDifferenz());
    }
}
