import com.sun.deploy.util.ArrayUtil;
import com.sun.tools.javac.util.ArrayUtils;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 *
 * �bungsklasse zu Feldern
 *
 * @version 1.0 vom 05.05.2011
 * @author Tenbusch
 */

public class Felder {

    //unsere Zahlenliste zum Ausprobieren
    private int[] zahlenliste = {5,8,4,3,9,1,2,7,6,0};

    //Konstruktor
    public Felder(){}

    //Methode die Sie implementieren sollen
    //ersetzen Sie den Befehl return 0 durch return ihre_Variable

    //die Methode soll die gr��te Zahl der Liste zur�ckgeben
    public int maxElement(){
        int number = 0;
        for (int i : zahlenliste) number = number < i ? i : number;
        return number;
    }

    //die Methode soll die kleinste Zahl der Liste zur�ckgeben
    public int minElement(){
        int number = zahlenliste[0];
        for (int i : zahlenliste) number = number > i ? i : number;
        return number;
    }

    //die Methode soll den abgerundeten Durchschnitt aller Zahlen zur�ckgeben
    public int durchschnitt(){
        int number = 0;
        for (int i : zahlenliste) number += i;
        return number / zahlenliste.length;
    }

    //die Methode soll die Anzahl der Elemente zur�ckgeben
    //der Befehl zahlenliste.length; k�nnte hierbei hilfreich sein
    public int anzahlElemente(){
        return zahlenliste.length;
    }

    //die Methode soll die Liste ausgeben
    public String toString(){
        String str = "";
        for (int i = 0; i < zahlenliste.length; i++) {
            if (i == zahlenliste.length-1) {
                str += zahlenliste[i];
            } else {
                str += zahlenliste[i] + ", ";
            }
        }
        return str;
    }

    //die Methode soll einen booleschen Wert zur�ckgeben, ob der Parameter in
    //dem Feld vorhanden ist
    public boolean istElement(int zahl){
        for (int i : zahlenliste) if (i == zahl) return true;
        return false;
    }

    //die Methode soll das erste Vorkommen der
    //als Parameter �bergebenen  Zahl liefern oder -1 bei nicht vorhanden
    public int getErstePosition(int zahl){
        for (int i = 0; i < zahlenliste.length; i++) return i;
        return -1;
    }

    //die Methode soll die Liste aufsteigend sortieren
    //googlen sie mal nach Array.sort() ;)
    public void sortiere(){
        Arrays.sort(zahlenliste);
    }

    public static void main(String[] args) {
        Felder testenMeinerLoesung = new Felder();
        System.out.println(testenMeinerLoesung.maxElement());
        System.out.println(testenMeinerLoesung.minElement());
        System.out.println(testenMeinerLoesung.durchschnitt());
        System.out.println(testenMeinerLoesung.anzahlElemente());
        System.out.println(testenMeinerLoesung.toString());
        System.out.println(testenMeinerLoesung.istElement(9));
        System.out.println(testenMeinerLoesung.getErstePosition(5));
        testenMeinerLoesung.sortiere();
        System.out.println(testenMeinerLoesung.toString());
    }
}