package experimentprimzahlenAlgorithmuss;

public class Experiment {
    public static void main(String[] args) {
        Timer timer = new Timer();
        Timer xtimer = new Timer();
        xtimer.start();
        int[] testZahlen = { 2, 11, 74027, 10004941, 231134327, 1999000021 };
        long[] result = new long[testZahlen.length];
        for (int x = 0; x < testZahlen.length; x++) {
            for (int i = 0; i < 100; i++) {
                timer.start();
                Primzahlalgorithmus.isPrimeZahl(testZahlen[x]);
                timer.stop();
                result[x] += timer.getDifferenz();
            }
            result[x] = (long) result[x]/100l;
            System.out.println("Result for " + testZahlen[x] + " is " + result[x] + "ns");
        }

    }
}
