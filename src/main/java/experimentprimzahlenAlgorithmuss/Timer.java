package experimentprimzahlenAlgorithmuss;

public class Timer {
    private long start;
    private long stop;

    public void start() {
        this.start = System.nanoTime();
    }

    public void stop() {
        this.stop = System.nanoTime();
    }

    public long getDifferenz() {
        return this.stop - this.start;
    }

    public void reset() {
        this.stop = 0;
        this.start = 0;
    }
}
