package zahlensortierer;

import java.util.*;

/**
 * @author Christoph-Thomas Abs
 *
 */
public class Zahlensortierer {

	//TODO  
	// vervollst�ndigen Sie die main methode so, dass sie 3 Zahlen vom Benutzer 
	// einliest und die kleinste und gr��te Zahl ausgibt.
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		int[] zahlen = new int[3];
		for (int index=0; index < zahlen.length; index++) {
			System.out.print((index + 1) + ". Zahl: ");
			zahlen[index] = myScanner.nextInt();
		}
		Arrays.sort(zahlen);
		System.out.println("\n" + "low:" + zahlen[0] + "\n");
		System.out.println("middle:" + zahlen[1] + "\n");
		System.out.println("high:" + zahlen[2] + "\n");
		myScanner.close();
	}
}
