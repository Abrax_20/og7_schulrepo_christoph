import experimentprimzahlenAlgorithmuss.Timer;

public class BubbleSort {
    public static int[] generateList(int lenght) {
        int[] list = new int[lenght];
        for (int index = 0; index < list.length; index++) {
            list[index] = lenght - (index+1);
        }

        return list;
    }

    public static int[] bubbleSort(int[] array) {
        int temp;
        for(int i=1; i<array.length; i++) {
            boolean isSorted = true;
            for(int x=0; x<array.length-i; x++) {
                if (array[x] > array[x+1]) {
                    temp=  array[x];
                    array[x] = array[x+1];
                    array[x+1] = temp;
                    isSorted = false;
                }
            }

            if (isSorted)
                return array;

        }
        return array;
    }

    public static void main(String[] args) {
        int[] array;
        Timer timer = new Timer();
        array = generateList(10);
        timer.start();
        array = bubbleSort(array);
        timer.stop();
        System.out.println("Result at n=10: " + timer.getDifferenz());
        array = generateList(100);
        timer.start();
        array = bubbleSort(array);
        timer.stop();
        System.out.println("Result at n=100: " + timer.getDifferenz());
        array = generateList(1000);
        timer.start();
        array = bubbleSort(array);
        timer.stop();
        System.out.println("Result at n=1000: " + timer.getDifferenz());
        array = generateList(10000);
        timer.start();
        array = bubbleSort(array);
        timer.stop();
        System.out.println("Result at n=10000: " + timer.getDifferenz());
        array = generateList(100000);
        timer.start();
        array = bubbleSort(array);
        timer.stop();
        System.out.println("Result at n=100000: " + timer.getDifferenz());
        System.exit(0);
    }
}
