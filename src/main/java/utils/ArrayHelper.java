package utils;

public class ArrayHelper {
    public static String convertArrayToString(int[] zahlen) {
        String str = "";
        for (int i = 0; i < zahlen.length; i++) {
            if (i == zahlen.length-1) {
                str += zahlen[i];
            } else {
                str += zahlen[i] + ", ";
            }
        }
        return str;
    }
}
