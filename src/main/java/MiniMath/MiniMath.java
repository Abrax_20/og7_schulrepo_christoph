package MintMath;

public class MiniMath {
	public int wieOftKlingelt(int anzahl) {
		if (anzahl == 1) return 1;

		return 0;
	}

	public int quersumme(int zahl) {
		if (zahl < 10) return zahl;
			return (zahl - (zahl/10)*10) + quersumme(zahl/10);
	}

	public int qsum(int x) {
		if (x > 1) {
			return x*x + qsum(x-1);
		}
		return 1;
	}

	public int add(int x, int y) {
		if (y==0) return x;
		return x+1+add(x, y-1);
	}

	/**
	 * Berechnet die Fakult�t einer Zahl (n!)
	 * @param n - Angabe der Zahl
	 * @return n!
	 */
	public static int berechneFakultaet(int n){
		return 0;
	}
	
	/**
	 * Berechnet die 2er-Potenz einer gegebenen Zahl
	 * @param n - Angabe der Potenz (max. 31 sonst Integeroverflow)
	 * @return 2^n 
	 */
	public static int berechneZweiHoch(int n){
		return 0;
	}
	
	/**
	 * Die Methode berechnet die Summe der Zahlen von 
	 * 1 bis n (also 1+2+...+(n-1)+n)
	 * @param n - ober Grenze der Aufsummierung
	 * @return Summe der Zahlen 1+2+...+(n-1)+n
	 */
	public static int berechneSumme(int n){
		return 0;
	}
	
}

