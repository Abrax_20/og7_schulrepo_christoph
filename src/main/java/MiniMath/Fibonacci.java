package MiniMath;

import java.util.Scanner;

public class Fibonacci {
    // Die ersten beiden Monate sind Konstant danach bilden sich die Monate anhand der beiden voherigen Monate Zusammen
    public static int fibo(int n) {
        // Ist n == 0 oder n < 0  dann gib 0 zurück (Monat 0)
        if (n <= 0) return 0;
        // Ist n == 1 dann gib 1 zurück (Monat 1)
        if (n == 1) return 1;
        // Addiere Letzten Monat mit vorletzten Monat zusammen
        return fibo(n - 1) + fibo(n-2);
    }

    public static void main(String[] args) {
        int n = 0;

        // Gib Int
        Scanner scanner = new Scanner(System.in);
        System.out.println("Gib eine Zahl ein: ");
        n = scanner.nextInt();

        // Ausgabe
        System.out.println("ANSWER: " + fibo(n));
    }
}
