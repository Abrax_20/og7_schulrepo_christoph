package Listen;

import java.util.*;
import java.util.function.Predicate;

public class StringList extends ArrayList<String> {
    private boolean sorted = false;
    private int ITEM_NOT_FOUND = -1;

    public boolean isSorted() {
        return sorted;
    }

    @Override
    public void sort(Comparator<? super String> c) {
        this.sorted = true;
        super.sort(c);
    }

    private String[] merge(String[] array, int l, int q, int r) {
        String[] arr = new String[array.length];
        int i, j;
        for (i = l; i <= q; i++) {
            arr[i] = array[i];
        }
        for (j = q + 1; j <= r; j++) {
            arr[r + q + 1 - j] = array[j];
        }
        i = l;
        j = r;
        for (int k = l; k <= r; k++) {
            if (arr[j].compareTo(arr[i]) >= 0) {
                array[k] = arr[i];
                i++;
            } else {
                array[k] = arr[j];
                j--;
            }
        }

        return array;
    }

/*    public int fastSearch(int von, int bis, String x, String[] array) {
        if (von <= bis) {
            // Von, bis -> INDEX
            // INDEX Builder
            int mb = (von + bis) / 2;
            int mi = (int) (von + ((bis - von) * ((x - array[von]) / (array[bis] - array[von]))));

            if (mb > mi) {
                int q = mb;
                mb = mi;
                mi = q;
            }
            if (x.equals(array[mb])) {
                return mb;
            }
            if (x.equals(array[mi])) {
                return mi;
            }
            if (x.compareTo(array[mb]) > 0) {
                return fastSearch(von, mi-1, x, array);
            }
            if (x.compareTo(array[mi]) > 0) {
                return fastSearch(mb+1, mi-1, x, array);
            }
            return fastSearch(mi+1, bis, x, array);
        }
        return ITEM_NOT_FOUND;
    }*/


    private String[] mergSort(String[] array, int l, int r) {
        if (l < r) {
            int q = (l + r) / 2;

            array = mergSort(array, l, q);
            array = mergSort(array, q + 1, r);
            array = merge(array, l, q, r);
        }
        return array;
    }



    public void sort() {
        if (this.sorted)
            return;
        String[] array = new String[this.size()];
        array = this.toArray(array);
        List<String> list = Arrays.asList(this.mergSort(array,  0, this.size()-1));
        this.clear();
        this.addAll(list);
    }


    @Override
    public boolean add(String e) {
        sorted = false;
        return super.add(e);
    }

    @Override
    public boolean remove(Object o) {
        sorted = false;
        return super.remove(o);
    }

    @Override
    public void add(int index, String element) {
        sorted = false;
        super.add(index, element);
    }

    @Override
    public String set(int index, String element) {
        sorted = false;
        return super.set(index, element);
    }

    @Override
    public boolean removeIf(Predicate<? super String> filter) {
        sorted = false;
        return super.removeIf(filter);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        sorted = false;
        return super.removeAll(c);
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        sorted = false;
        super.removeRange(fromIndex, toIndex);
    }

    @Override
    public String remove(int index) {
        sorted = false;
        return super.remove(index);
    }

    @Override
    public int indexOf(Object o) {
        if (!this.sorted)
            return super.indexOf(o);
        return -1;
    }
}
