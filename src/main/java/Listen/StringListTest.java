package Listen;

public class StringListTest {
    public static void main(String[] args) {
        StringList stringList = new StringList();
        stringList.add("ABC");
        stringList.add("DBC");
        stringList.add("RBC");
        stringList.add("CBC");
        stringList.add("ABC");
        stringList.add("QBC");
        stringList.sort();
        for (String s : stringList) {
            System.out.println(s);
        }
    }
}
