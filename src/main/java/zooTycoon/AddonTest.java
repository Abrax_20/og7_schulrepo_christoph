package zooTycoon;

import taschenrechner.Taschenrechner;

import java.util.Scanner;

public class AddonTest {
    public static Addon addon;

    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        Taschenrechner ts = new Taschenrechner();

        addon = new Addon(1, "Awsome Item", 0.99, 10, 5);
        int swValue;

        // Display menu graphics
        System.out.println("=============================");
        System.out.println("|   MENU SELECTION DEMO     |");
        System.out.println("=============================");
        System.out.println("| Options:                  |");
        System.out.println("|        1. Restart         |");
        System.out.println("|        2. Get Addon       |");
        System.out.println("|        3. Use             |");
        System.out.println("|        4. Buy             |");
        System.out.println("|        5. Get Money Value |");
        System.out.println("|        6. Exit            |");
        System.out.println("=============================");

        while(true) {
            System.out.print(" Select option: ");
            swValue = myScanner.next().charAt(0);
            // Switch construct
            switch (swValue) {
                case '1':
                    addon = new Addon(1, "Awsome Item", 0.99, 10, 5);
                    break;
                case '2':
                    System.out.println(addon.toString());
                    break;
                case '3':
                    addon.use();
                    break;
                case '4':
                    addon.buy();
                    break;
                case '5':
                    System.out.println(addon.getWorthMoney());
                    break;
                case '6':
                    System.out.println("Bye");
                    System.exit(0);
                default:
                    System.out.println("Invalid selection");
            }
        }
    }
}
