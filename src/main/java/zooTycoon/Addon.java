package zooTycoon;

public class Addon {
    private int id = 0;
    private int max = 0;
    private double price = 0;
    private String title = "";
    private int inInventory = 0;

    public Addon(int id) {
        this.id = id;
    }
    public Addon(int id, String title) {
        this.id = id;
        this.title = title;
    }
    public Addon(int id, String title, double price) {
        this.id = id;
        this.title = title;
        this.price = price;
    }
    public Addon(int id, String title, double price, int max) {
        this.id = id;
        this.max = max;
        this.title = title;
        this.price = price;
    }
    public Addon(int id, String title, double price, int max, int inInventory) {
        this.id = id;
        this.max = max;
        this.title = title;
        this.price = price;
        this.inInventory = inInventory;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public int getMax() {
        return max;
    }

    public int getInInventory() {
        return inInventory;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public void setInInventory(int inInventory) {
        this.inInventory = inInventory;
    }

    public void buy() {
        if (this.inInventory < this.max) {
            this.inInventory++;
        }
    }

    public void use() {
        if (this.inInventory > 0) {
            this.inInventory--;
        }
    }

    public double getWorthMoney() {
        return this.inInventory * this.price;
    }

    @Override
    public String toString() {
        return "id: " + this.id + "\n"
            + "title: " + this.title + "\n"
            + "price: " + this.price + "\n"
            + "max: " + this.max + "\n"
            + "inInventory: " + this.inInventory + "\n";
    }
}
