package keystore;

import java.util.ArrayList;

public class Keystore02 extends ArrayList<String> {
    @Override
    public String toString() {
        String string = "";
        for (String s : this) {
            string = s + ", ";
        }
        return string;
    }
}
