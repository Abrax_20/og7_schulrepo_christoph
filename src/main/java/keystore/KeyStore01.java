package keystore;

public class KeyStore01 {
    private int lenght = 100;
    private String[] keystore;


    public KeyStore01() {
        this.keystore = new String[this.lenght];
    }

    public KeyStore01(int lenght) {
        this.lenght = lenght;
        this.keystore = new String[this.lenght];
    }

    public void clear() {
        for (int i = 0; i < this.keystore.length; i++) {
            this.keystore[i] = null;
        }
    }


    public boolean add(String string) {
        for (int i = 0; i < this.keystore.length; i++) {
            if (this.keystore[i] == null) {
                this.keystore[i] = string;
                return true;
            }
        }
        return false;
    }


    public String get(int index) {
        try {
            String string = this.keystore[index];
            return string;
        } catch (Exception e) {
            return null;
        }
    }

    public int indexOf(String string) {
        for (int i = 0; i < this.keystore.length; i++) {
            if (this.keystore[i].equals(string)) {
                return i;
            }
        }
        return -1;
    }

    public boolean remove(int index) {
        try {
            this.keystore[index] = null;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean remove(String string) {
        for (int i = 0; i < this.keystore.length; i++) {
            if (this.keystore[i].equals(string)) {
                this.keystore[i] = null;
                return true;
            }
        }
        return false;
    }


    public int size() {
        int counter = 0;
        for (int i = 0; i < this.keystore.length; i++) {
            if (this.keystore[i] != null)
                counter++;
        }
        return counter;
    }

    @Override
    public String toString() {
        String string = "";
        for (int i = 0; i < this.keystore.length; i++) {
            if (this.keystore[i] != null) {
                if (string.length() == 0) {
                    string = this.keystore[i];
                } else {
                    string += ", " + this.keystore[i];
                }
            }
        }
        return string;
    }
}
