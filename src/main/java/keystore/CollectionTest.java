package keystore;

import keystore.Buch;

import java.util.*;

public class CollectionTest {
    static void menue() {
        System.out.println("\n ***** Buch-Verwaltung *******");
        System.out.println(" 1) eintragen ");
        System.out.println(" 2) finden ");
        System.out.println(" 3) l�schen");
        System.out.println(" 4) Die gr��te ISBN");
        System.out.println(" 5) zeigen");
        System.out.println(" 9) Beenden");
        System.out.println(" ********************");
        System.out.print(" Bitte die Auswahl treffen: ");
    } // menue

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        List<Buch> buchliste = new LinkedList<Buch>();
        Scanner myScanner = new Scanner(System.in);

        char wahl;
        Buch result;
        String eintrag;

        int index;
        do {
            menue();
            String isbn;
            wahl = myScanner.next().charAt(0);
            switch (wahl) {
                case '1':
                    System.out.println("ISBN: ");
                    isbn = myScanner.next();
                    System.out.println("Title: ");
                    String tittle = myScanner.next();
                    System.out.println("Autor: ");
                    String autor = myScanner.next();
                    buchliste.add(new Buch(autor, tittle, isbn));
                    break;
                case '2':
                    System.out.println("ISBN: ");
                    isbn = myScanner.next();
                    result = findeBuch(buchliste, isbn);
                    if (result != null) {
                        System.out.println("------------------------------------------------");
                        System.out.println("Title: " + result.getTitel());
                        System.out.println("Auto: " + result.getAutor());
                        System.out.println("ISBN: " + result.getIsbn());
                        System.out.println("------------------------------------------------");
                        result = null;
                    } else {
                        System.out.println("");
                    }
                    break;
                case '3':
                    System.out.println("ISBN: ");
                    isbn = myScanner.next();
                    result = findeBuch(buchliste, isbn);
                    if (result != null) {
                        System.out.println("Gelöscht " + loescheBuch(result, buchliste));
                        result = null;
                    } else {
                        System.out.println("Nicht Gefunden");
                    }
                    break;
                case '4':
                    System.out.println(ermitteleGroessteISBN(buchliste));
                    break;
                case '5':
                    for (Buch buch : buchliste) {
                        System.out.println(buch.toString());
                    }
                    break;
                case '9':
                    System.exit(0);
                    break;
                default:
                    menue();
                    wahl = myScanner.next().charAt(0);
            } // switch

        } while (wahl != 9);
    }// main

    public static Buch findeBuch(List<Buch> buchliste, String isbn) {
        Buch searchBook = new Buch("","",isbn);
        if (buchliste.contains(searchBook))
            return buchliste.get(buchliste.indexOf(searchBook));
        return null;
    }

    public static boolean loescheBuch(Buch buch, List<Buch> buchList) {
        return buchList.remove(buch);
    }

    public static String ermitteleGroessteISBN(List<Buch> buchList) {
        return buchList.get(0).getIsbn();
	}

}