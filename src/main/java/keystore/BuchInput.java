package keystore;

import java.util.*;

public class BuchInput {
    public static ArrayList<Buch> list = new ArrayList<Buch>();

    public static Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {
        int index;
        String input, isbn, title, author;
        while (true) {
            input = scanner.next();
            switch (input) {
                case "add": {
                    System.out.println("ISBN");
                    isbn = scanner.next();
                    System.out.println("Title: ");
                    title = scanner.next();
                    System.out.println("Author: ");
                    author = scanner.next();
                    list.add(new Buch(author, title, isbn));
                    break;
                }
                case "remove": {
                    System.out.println("Index: ");
                    index = scanner.nextInt();
                    list.remove(index);
                    break;
                }
                case "get": {
                    for (Buch buch : list) {
                        System.out.println(buch.toString());
                    }
                    break;
                }
                case "sort": {
                    Collections.sort(list);
                    break;
                }
                default: {
                    System.out.println("help");
                }
            }
        }
    }
}
