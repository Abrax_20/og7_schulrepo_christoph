package OSZ_Kickers;

public class Trainer extends Person {
    private String spielklasse;
    private char lizenzklasse;
    private double aufwandsentschaedigung;

    public Trainer(String name) {
        this.name = name;
    }

    public Trainer(String name, String telefonNummer) {
        this.name = name;
        this.telefonNummer = telefonNummer;
    }

    public Trainer(String name, String telefonNummer, String spielklasse) {
        this.name = name;
        this.spielklasse = spielklasse;
        this.telefonNummer = telefonNummer;
    }

    public Trainer(String name, String telefonNummer, String spielklasse, char lizenzklasse) {
        this.name = name;
        this.spielklasse = spielklasse;
        this.lizenzklasse = lizenzklasse;
        this.telefonNummer = telefonNummer;
    }

    public Trainer(String name, String telefonNummer, String spielklasse, char lizenzklasse, double aufwandsentschaedigung) {
        this.name = name;
        this.spielklasse = spielklasse;
        this.lizenzklasse = lizenzklasse;
        this.telefonNummer = telefonNummer;
        this.aufwandsentschaedigung = aufwandsentschaedigung;
    }

    public String getSpielklasse() {
        return spielklasse;
    }

    public void setSpielklasse(String spielklasse) {
        this.spielklasse = spielklasse;
    }

    public char getLizenzklasse() {
        return lizenzklasse;
    }

    public void setLizenzklasse(char lizenzklasse) {
        this.lizenzklasse = lizenzklasse;
    }

    public double getAufwandsentschaedigung() {
        return aufwandsentschaedigung;
    }

    public void setAufwandsentschaedigung(double aufwandsentschaedigung) {
        this.aufwandsentschaedigung = aufwandsentschaedigung;
    }
}
