package OSZ_Kickers;

public class Mannschaftsleiter extends Spieler {
    private int rabat;
    private String manschaftsname;

    public Mannschaftsleiter(String name) {
        super(name);
    }

    public Mannschaftsleiter(String name, String telefonNummer) {
        super(name, telefonNummer);
    }

    public Mannschaftsleiter(String name, String telefonNummer, int number) {
        super(name, telefonNummer, number);
    }

    public Mannschaftsleiter(String name, String telefonNummer, int number, int position) {
        super(name, telefonNummer, number, position);
    }

    public Mannschaftsleiter(String name, String telefonNummer, int number, int position, int rabat) {
        super(name, telefonNummer, number, position);
        this.rabat = rabat;
    }

    public Mannschaftsleiter(String name, String telefonNummer, int number, int position, int rabat, String manschaftsname) {
        super(name, telefonNummer, number, position);
        this.rabat = rabat;
        this.manschaftsname = manschaftsname;
    }

    public String getManschaftsname() {
        return manschaftsname;
    }

    public void setManschaftsname(String manschaftsname) {
        this.manschaftsname = manschaftsname;
    }

    public int getRabat() {
        return rabat;
    }

    public void setRabat(int rabat) {
        this.rabat = rabat;
    }
}
