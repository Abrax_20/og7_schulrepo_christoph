package OSZ_Kickers;

public class Spieler extends Mitglied {
    private int number;
    private int position;

    public Spieler(String name) {
        this.name = name;
    }

    public Spieler(String name, String telefonNummer) {
        this.name = name;
        this.telefonNummer = telefonNummer;
    }

    public Spieler(String name, String telefonNummer, int number) {
        this.name = name;
        this.number = number;
        this.telefonNummer = telefonNummer;
    }

    public Spieler(String name, String telefonNummer, int number, int position) {
        this.name = name;
        this.number = number;
        this.position = position;
        this.telefonNummer = telefonNummer;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
