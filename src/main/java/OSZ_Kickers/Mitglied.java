package OSZ_Kickers;

public abstract class Mitglied extends Person {
    protected boolean hatJahresBeitragBezahlt;

    public boolean isHatJahresBeitragBezahlt() {
        return hatJahresBeitragBezahlt;
    }

    public void setHatJahresBeitragBezahlt(boolean hatJahresBeitragBezahlt) {
        this.hatJahresBeitragBezahlt = hatJahresBeitragBezahlt;
    }
}
