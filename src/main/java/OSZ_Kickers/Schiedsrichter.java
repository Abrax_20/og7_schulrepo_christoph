package OSZ_Kickers;

public class Schiedsrichter extends Person {
    private int numberOfGames;

    public Schiedsrichter(String name) {
        this.name = name;
    }

    public Schiedsrichter(String name, String telefonNummer) {
        this.name = name;
        this.telefonNummer = telefonNummer;
    }

    public Schiedsrichter(String name, String telefonNummer, int numberOfGames) {
        this.name = name;
        this.telefonNummer = telefonNummer;
        this.numberOfGames = numberOfGames;
    }

    public int getNumberOfGames() {
        return numberOfGames;
    }

    public void setNumberOfGames(int numberOfGames) {
        this.numberOfGames = numberOfGames;
    }
}
