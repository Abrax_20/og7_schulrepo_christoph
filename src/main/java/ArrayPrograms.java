import utils.ArrayHelper;

import java.util.Scanner;

public class ArrayPrograms {
    public static Scanner scanner = new Scanner(System.in);

    public static void programm1() {
        int[] array = new int[20];
        for (int i = 0; i < array.length; i++) {
            array[i] = 1;
        }
        System.out.println(ArrayHelper.convertArrayToString(array));
    }

    public static void programm2() {
        int[] array = new int[16];
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }
        System.out.println(ArrayHelper.convertArrayToString(array));
    }

    public static void programm3() {
        int[] array = new int[17];
        for (int i = 0; i < array.length; i++) {
            array[i] = array.length-1-i;
        }
        System.out.println(ArrayHelper.convertArrayToString(array));
    }

    public static void programm4() {
        int[] array = new int[5];
        for (int i = 0; i < array.length; i++) {
            System.out.println("Gib eine zahl ein:");
            array[i] = scanner.nextInt();
        }
        System.out.println(ArrayHelper.convertArrayToString(array));
    }

    public static void programm5() {
        System.out.println("Gib eine zahl ein:");
        int length = scanner.nextInt();
        int[] array = new int[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }
        System.out.println(ArrayHelper.convertArrayToString(array));
    }

    public static void programm6() {
        System.out.println("Gib eine zahl ein:");
        int x = scanner.nextInt();
        System.out.println("Gib eine zahl ein:");
        int y = scanner.nextInt();
        int[][] array = new int[y][x];
        for (int index1 = 0; index1 < array.length; index1++) {
            for (int index2 = 0; index2 < array[index1].length; index2++) {
                array[index1][index2] = index2;
                System.out.print(array[index1][index2] + ",");
            }
            System.out.print("\n");
        }
    }

    public static void main(String[] args) {
        while (true) {
            System.out.println("Program Code: ");
            switch (scanner.next()) {
                case "0":
                    System.exit(0);
                    break;
                case "1":
                    programm1();
                    break;
                case "2":
                    programm2();
                    break;
                case "3":
                    programm3();
                    break;
                case "4":
                    programm4();
                    break;
                case "5":
                    programm5();
                    break;
                case "6":
                    programm6();
                    break;
            }
        }
    }
}
