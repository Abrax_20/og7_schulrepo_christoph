import experimentprimzahlenAlgorithmuss.Timer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class ListTest {
    public static void main(String[] args) {
        Timer timer = new Timer();
        ArrayList<Integer> zahlen01 = new ArrayList<Integer>();
        Vector<Integer> zahlen02 = new Vector<Integer>();
        LinkedList<Integer> zahlen03 = new LinkedList<Integer>();

        // add
        timer.start();
        for (int x = 0; x < 1000; x++) {
            zahlen01.add(x);
        }
        timer.stop();
        System.out.println("ArrayList: " + timer.getDifferenz()/1000);
        timer.start();
        for (int x = 0; x < 1000; x++) {
            zahlen02.add(x);
        }
        timer.stop();
        System.out.println("Vector: " + timer.getDifferenz()/1000);
        timer.start();
        for (int x = 0; x < 1000; x++) {
            zahlen03.add(x);
        }
        timer.stop();
        System.out.println("LinkedList: " + timer.getDifferenz()/1000);

        // indexOf
        timer.start();
        for (int x = 0; x < 1000; x++) {
            zahlen01.indexOf(x);
        }
        timer.stop();
        System.out.println("ArrayList: " + timer.getDifferenz()/1000);
        timer.start();
        for (int x = 0; x < 1000; x++) {
            zahlen02.indexOf(x);
        }
        timer.stop();
        System.out.println("Vector: " + timer.getDifferenz()/1000);
        timer.start();
        for (int x = 0; x < 1000; x++) {
            zahlen03.indexOf(x);
        }
        timer.stop();
        System.out.println("LinkedList: " + timer.getDifferenz()/1000);

        // Remove
        timer.start();
        for (int x = 0; x < 1000; x++) {
            zahlen01.remove(x);
        }
        timer.stop();
        System.out.println("ArrayList: " + timer.getDifferenz()/1000);
        timer.start();
        for (int x = 0; x < 1000; x++) {
            System.out.println(x);
            System.out.println(zahlen01.size());
            //zahlen02.remove(x);
        }
        timer.stop();
        System.out.println("Vector: " + timer.getDifferenz()/1000);
        timer.start();
        for (int x = 0; x < 1000; x++) {
            zahlen03.remove(x);
        }
        timer.stop();
        System.out.println("LinkedList: " + timer.getDifferenz()/1000);
    }
}
