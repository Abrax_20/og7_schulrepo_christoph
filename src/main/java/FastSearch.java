import experimentprimzahlenAlgorithmuss.Timer;

import java.util.Random;

public class FastSearch {

    private static final int[] ARRAY_OF_LIST_LENGTH = {15000000, 16000000, 17000000, 18000000, 19000000, 20000000};
    private static final int ANZAHL_DURCHGAENGE = 10;
    private static int ITEM_NOT_FOUND = -1;
    private static long[] generateList(int length) {
        long[] list = new long[length];
        Random generator = new Random();
        for (int i = 0; i < list.length; i++) {
            list[i] = i + i == 0 ? 1 : list[i-1] + generator.nextInt(100);
        }
        return list;
    }

    public static long fastSearch(int von, int bis, long x, long[] array) {
        if (von <= bis) {
            // Von, bis -> INDEX
            // INDEX Builder
            int mb = (von + bis) / 2;
            int mi = (int) (von + ((bis - von) * ((x - array[von]) / (array[bis] - array[von]))));

            if (mb > mi) {
                int q = mb;
                mb = mi;
                mi = q;
            }
            if (x == array[mb]) {
                return mb;
            }
            if (x == array[mi]) {
                return mi;
            }
            if (x < array[mb]) {
                return fastSearch(von, mi-1, x, array);
            }
            if (x < array[mi]) {
                return fastSearch(mb+1, mi-1, x, array);
            }
            return fastSearch(mi+1, bis, x, array);
        }
        return ITEM_NOT_FOUND;
    }

    public static int linearSearch(long x, long[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == x) {
                return i;
            }
        }

        return ITEM_NOT_FOUND;
    }

    public static void searchExperiment(int n) {
        long[] list = generateList(n);

        Timer timer = new Timer();

        System.out.println("    Best case:");

        // FastSearch, Linear Search best case
        timer.start();
        for (int i = 0; i < ANZAHL_DURCHGAENGE; i++)
            fastSearch(0, n-1, list[0], list);
        timer.stop();

        System.out.println("        FastSearch: " + (timer.getDifferenz() / ANZAHL_DURCHGAENGE));

        timer.start();
        for (int i = 0; i < ANZAHL_DURCHGAENGE; i++)
            linearSearch((int) list[0], list);
        timer.stop();

        System.out.println("        Linear Search: " + (timer.getDifferenz() / ANZAHL_DURCHGAENGE));


        // Linear Search worst case
        System.out.println("    worst case of Linear Search:");
        timer.start();
        for (int i = 0; i < ANZAHL_DURCHGAENGE; i++)
            fastSearch(0, n-1, list[n-1], list);
        timer.stop();

        System.out.println("        Linear Search: " + (timer.getDifferenz() / ANZAHL_DURCHGAENGE));


        timer.start();
        for (int i = 0; i < ANZAHL_DURCHGAENGE; i++)
            linearSearch((int) list[n-1], list);
        timer.stop();
        System.out.println("    Fast Search: " + (timer.getDifferenz() / ANZAHL_DURCHGAENGE));


        // FastSearch, Linear Search average case
        System.out.println("    Average case of Linear Search:");
        timer.start();
        for (int i = 0; i < ANZAHL_DURCHGAENGE; i++)
            fastSearch(0, n-1, list[((n-1)/2)-1], list);
        timer.stop();
        System.out.println("        Fast Search: " + (timer.getDifferenz() / ANZAHL_DURCHGAENGE));



        timer.start();
        for (int i = 0; i < ANZAHL_DURCHGAENGE; i++)
            linearSearch((int) list[((n-1)/2)-1], list);
        timer.stop();
        System.out.println("    Linear Search: " + (timer.getDifferenz() / ANZAHL_DURCHGAENGE));


        System.out.println("    ITEM_NOT_FOUND and Average case of Fast Search:");
        // FastSearch, Linear Search ITEM NOT FOUND ERROR this is the FastSearch worst case
        timer.start();
        for (int i = 0; i < ANZAHL_DURCHGAENGE; i++)
            timer.stop();
        System.out.println("        Fast Search: " + (timer.getDifferenz() / ANZAHL_DURCHGAENGE));

        timer.start();
        for (int i = 0; i < ANZAHL_DURCHGAENGE; i++)
            linearSearch(list[n-1]+1, list);
        timer.stop();
        System.out.println("        Linear Search: " + (timer.getDifferenz() / ANZAHL_DURCHGAENGE));
    }


    public static void main(String[] args) {
        for (int i = 0; i < ARRAY_OF_LIST_LENGTH.length; i++) {
            System.out.println("case n = " + ARRAY_OF_LIST_LENGTH[i]);
            searchExperiment(ARRAY_OF_LIST_LENGTH[i]);
        }    
    }
}
