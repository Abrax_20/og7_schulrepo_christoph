package omnom;

public class Haustier {
    private int muede = 100;
    private int hunger = 100;
    private int gesund = 100;
    private String name = "";
    private int zufrieden = 100;

    public Haustier() {}
    public Haustier(String name) {
        this.name = name;
    }

    private int validationOfLifeValues(int lifeValue) {
        if (lifeValue >= 0 && lifeValue <= 100) {
            return lifeValue;
        }

        if (lifeValue > 100) {
            return 100;
        }

        return 0;
    }

    public void heilen() {
        this.gesund = 100;
    }

    public void spielen(int spiel) {
        this.zufrieden = validationOfLifeValues(this.zufrieden + spiel);
    }

    public void schlafen(int schlaf) {
        this.muede = validationOfLifeValues(this.muede + schlaf);
    }

    public void fuettern(int futter) {
        this.hunger = validationOfLifeValues(this.hunger + futter);
    }

    public int getHunger() {
        return hunger;
    }

    public int getMuede() {
        return muede;
    }

    public int getGesund() {
        return gesund;
    }

    public String getName() {
        return name;
    }

    public int getZufrieden() {
        return zufrieden;
    }

    public void setHunger(int hunger) {
        this.hunger = validationOfLifeValues(hunger);
    }

    public void setMuede(int muede) {
        this.muede = validationOfLifeValues(muede);
    }

    public void setGesund(int gesund) {
        this.gesund = validationOfLifeValues(gesund);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setZufrieden(int zufrieden) {
        this.zufrieden = validationOfLifeValues(zufrieden);
    }
}
