package OSZ_Kickers;

import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

class TestOSZKicker {
    @Test
    void Test() {
        Trainer trainer = new Trainer("trainer");
        Spieler spieler01 = new Spieler("spieler01");
        Schiedsrichter schiedsrichter = new Schiedsrichter("schiedsrichter");
        Mannschaftsleiter mannschaftsleiter = new Mannschaftsleiter("mannschaftsleiter");

        spieler01.setName("Test");
        spieler01.setPosition(0);
        spieler01.setNumber(0);
        spieler01.setTelefonNummer("111111");

        assert (spieler01.getName().equals("Test"));
        assert (spieler01.getPosition() == 0);
        assert (spieler01.getNumber() == 0);
        assert (spieler01.getTelefonNummer().equals("111111"));

        schiedsrichter.setName("Test");
        schiedsrichter.setTelefonNummer("111111");
        schiedsrichter.setNumberOfGames(10);
        assert (schiedsrichter.getName().equals("Test"));
        assert (schiedsrichter.getTelefonNummer().equals("111111"));
        assert (schiedsrichter.getNumberOfGames() == 10);

        mannschaftsleiter.setName("Test");
        mannschaftsleiter.setPosition(0);
        mannschaftsleiter.setNumber(0);
        mannschaftsleiter.setTelefonNummer("111111");
        mannschaftsleiter.setRabat(10);
        mannschaftsleiter.setManschaftsname("HALLO");

        assert (mannschaftsleiter.getName().equals("Test"));
        assert (mannschaftsleiter.getManschaftsname().equals("HALLO"));
        assert (mannschaftsleiter.getRabat() == 10);
        assert (mannschaftsleiter.getPosition() == 0);
        assert (mannschaftsleiter.getNumber() == 0);
        assert (mannschaftsleiter.getTelefonNummer().equals("111111"));


        trainer.setAufwandsentschaedigung(160);
        trainer.setName("Test");
        trainer.setTelefonNummer("111111");
        trainer.setLizenzklasse('A');
        trainer.setSpielklasse("Test");

        assert (trainer.getName().equals("Test"));
        assert (trainer.getSpielklasse().equals("Test"));
        assert (trainer.getLizenzklasse() == 'A');
        assert (trainer.getAufwandsentschaedigung() == 160);
        assert (trainer.getTelefonNummer().equals("111111"));
    }
}