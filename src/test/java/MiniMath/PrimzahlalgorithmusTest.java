package MiniMath;

import experimentprimzahlenAlgorithmuss.Primzahlalgorithmus;
import experimentprimzahlenAlgorithmuss.Timer;
import org.junit.jupiter.api.Test;

class PrimzahlalgorithmusTest {

    @Test
    void isPrimeZahl() {
        assert(Primzahlalgorithmus.isPrimeZahl(1) == false);
        assert(Primzahlalgorithmus.isPrimeZahl(2) == true);
        assert(Primzahlalgorithmus.isPrimeZahl(3) == true);
        assert(Primzahlalgorithmus.isPrimeZahl(4) == false);
        assert(Primzahlalgorithmus.isPrimeZahl(5) == true);
        assert(Primzahlalgorithmus.isPrimeZahl(6) == false);
        assert(Primzahlalgorithmus.isPrimeZahl(7) == true);
        assert(Primzahlalgorithmus.isPrimeZahl(8) == false);
        assert(Primzahlalgorithmus.isPrimeZahl(9) == false);
        assert(Primzahlalgorithmus.isPrimeZahl(10) == false);
        assert(Primzahlalgorithmus.isPrimeZahl(11) == true);
        assert(Primzahlalgorithmus.isPrimeZahl(12) == false);
    }
}