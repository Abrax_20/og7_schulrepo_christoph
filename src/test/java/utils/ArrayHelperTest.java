package utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayHelperTest {

    @Test
    void convertArrayToString() {
        int[] array = { 1, 2, 3 };
        assert(ArrayHelper.convertArrayToString(array).equals("1, 2, 3"));
    }
}